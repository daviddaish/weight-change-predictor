import weight_predition
import pandas as pd

# === Define mock user entered data ===
user_entered_data = {
    'weight': '88.0kg',
    'height': '183.0cm',
    'physical_activity_level': (1.5167, 1.6333),
    'body_fat_percentage': '20',
    'gender': 'male',
    'birth_date': '7th feb 1997',
    'goal_caloric_difference': 0.8
}

exportable_dataframe, unit_dict, dimensionality_dict = weight_predition.predict_weight(
    user_entered_data['weight'],
    user_entered_data['height'],
    user_entered_data['physical_activity_level'][0],
    user_entered_data['physical_activity_level'][1],
    user_entered_data['gender'],
    user_entered_data['birth_date'],
    user_entered_data['goal_caloric_difference']
)

exportable_dataframe.to_csv('./output.csv')
