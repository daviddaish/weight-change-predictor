# Weight change predictor

This is a work in progress project to create a weight loss predictor that is
scientifically rigorous, includes a range of estimates, can export to a csv,
and is usable programmatically.

## Problems
* Don't know what determines what % of energy is recouped from muscle and fat.
* No equation for bulking
* No way to input user values.
* No graphing/visual summary.
* No known way to compare predictions to real values.
* No way to use it with a body fat %.
* No meal plan provided.
