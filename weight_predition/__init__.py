# Python 3.6.4

# === Prep for script work with imports and definitions ===
# Import modules
import pandas as pd

# Import custom modules
from weight_predition.registry import ureg
from . import biometric
from . import validate


def predict_weight (
    weight, height, lower_pal, upper_pal, gender, birth_date, goal_caloric_difference,
    body_fat_percentage=False, start_date='now', calc_period='1 week', calc_length='1 year'):


    # === Get validated and converted values from user entered data ===
    # Pint mass measure
    ingested_weight = validate.get_weight(weight)

    # Pint length measure
    ingested_height = validate.get_height(height)

    # Tuple, length of two, with floats
    ingested_physical_activity_levels = validate.get_physical_activity_levels(lower_pal, upper_pal)

    # String, either 'male' or 'female'
    ingested_gender = validate.get_gender(gender)

    # Pint time measure
    ingested_age = validate.get_age(birth_date)

    # Float
    ingested_goal_caloric_difference = validate.get_goal(goal_caloric_difference)

    # Either False boolean (if not known), or float
    ingested_body_fat_percentage = validate.get_body_fat_percentage(body_fat_percentage)

    # Python datetime object
    ingested_start_date = validate.get_start_date(start_date)

    # Pint time measure
    ingested_calc_period = validate.get_calc_period(calc_period)

    # Pint time measure
    ingested_calc_length = validate.get_calc_length(calc_length)


    # Define function for getting inital values, without body fat percentage
    # def get_inital_values(goal, weight, height, gender, years_alive, activity_level, date, period):
    def get_inital_values(
        weight, height, physical_activity_levels, gender, age,
        goal_caloric_difference, body_fat_percentage, start_date, calc_period, calc_length):

        if body_fat_percentage:
            raise ValueError('I have not yet developed equations that incorporate body fat percentage.')

        # Calculate body mass index
        body_mass_index = biometric.get_body_mass_index(weight, height)

        # Calculate basal metabolic rate estimates
        mean_basal_metabolic_rate_estimate, basal_metabolic_rate_estimates = biometric.get_bmr(
            weight, height, age, gender
        )

        # Define physical activity level estimates
        physical_activity_level_estimates = physical_activity_levels

        # Calculate TDEE estimates
        mean_tdee_estimate, tdee_estimates = biometric.calulate_tdee_estimates(
            basal_metabolic_rate_estimates, physical_activity_level_estimates
        )

        # Calculate goal energy intake
        goal_energy_intake = biometric.calculate_goal_energy_intake(
            mean_tdee_estimate, goal_caloric_difference
        )

        final_date = start_date + calc_length

        dataframe = pd.DataFrame(columns=[
            'Goal, caloric difference',
            'Weight', 'Height', 'Gender', 'Years alive',
            'Activity level, upper', 'Activity level, lower',
            'Basal metabolic rate, mean', 'Basal metabolic rate, upper', 'Basal metabolic rate, lower',
            'TDEE, mean', 'TDEE, upper', 'TDEE, lower',
            'Rate of weight change, mean', 'Rate of weight change, upper', 'Rate of weight change, lower',
            'Weight changed during period, mean', 'Weight changed during period, upper', 'Weight changed during period, lower',
            'Energy intake', 'BMI'
        ])

        return (
            goal_caloric_difference, goal_energy_intake,
            weight, height, gender, age, physical_activity_levels,
            start_date, calc_period, final_date, dataframe
        )

    # Define function for running a single step of the prediction
    # def predict_weight_change(goal, energy_intake, weight, height, gender, years_alive, activity_level, date, period, final_date, dataframe):
    def predict_weight_change(goal_caloric_difference, energy_intake,
        weight, height, gender, age, physical_activity_levels,
        date, calc_period, final_date, dataframe):

        # Calculate basal metabolic rate estimates
        mean_basal_metabolic_rate_estimate, basal_metabolic_rate_estimates = biometric.get_bmr(
            weight, height, age, gender
        )

        # Define physical activity level estimates
        physical_activity_level_estimates = physical_activity_levels

        # Calculate TDEE estimates
        mean_tdee_estimate, tdee_estimates = biometric.calulate_tdee_estimates(
            basal_metabolic_rate_estimates, physical_activity_level_estimates
        )

        # Define weight change and rate of weight change estimates
        mean_weight_change, mean_weight_change_rate = biometric.calculate_change_of_weight(mean_tdee_estimate, energy_intake, calc_period)
        upper_weight_change, upper_weight_change_rate = biometric.calculate_change_of_weight(max(tdee_estimates), energy_intake, calc_period)
        lower_weight_change, lower_weight_change_rate = biometric.calculate_change_of_weight(min(tdee_estimates), energy_intake, calc_period)

        # Define stats at end of period
        end_weight = weight + mean_weight_change
        end_height = height
        end_bmi = biometric.get_body_mass_index(end_weight, end_height)
        end_gender = gender
        end_age = age + calc_period
        end_date = date + calc_period

        # Define values to be entered into the pandas dataframe. These values are
        # equal to the prediction for their real world values at the date specified
        # in variable: end_date.
        output_values = {
            'Goal, caloric difference': goal_caloric_difference,
            'Weight': end_weight,
            'Height': end_height,
            'Gender': end_gender,
            'Years alive': end_age,
            'Activity level, upper': max(physical_activity_level_estimates),
            'Activity level, lower': min(physical_activity_level_estimates),
            'Basal metabolic rate, mean': mean_basal_metabolic_rate_estimate,
            'Basal metabolic rate, upper': max(basal_metabolic_rate_estimates),
            'Basal metabolic rate, lower': min(basal_metabolic_rate_estimates),
            'TDEE, mean': mean_tdee_estimate,
            'TDEE, upper': max(tdee_estimates),
            'TDEE, lower': min(tdee_estimates),
            'Rate of weight change, mean': mean_weight_change_rate,
            'Rate of weight change, upper': upper_weight_change_rate,
            'Rate of weight change, lower': lower_weight_change_rate,
            'Weight changed during period, mean': mean_weight_change,
            'Weight changed during period, upper': upper_weight_change,
            'Weight changed during period, lower': lower_weight_change,
            'Energy intake': energy_intake,
            'BMI': end_bmi,
        }

        # Add the output values to the running dataframe
        dataframe.loc[end_date] = output_values

        # Define values to feed back into recurring function
        input_values = (
            goal_caloric_difference, energy_intake,
            end_weight, end_height, end_gender, end_age, physical_activity_levels,
            end_date, calc_period, final_date, dataframe
        )

        # If the date for this calculation is after the final date specfied to
        # conclude the predictions, end the recursion, and return the dataframe
        if end_date >= final_date:
            return dataframe

        # If the final date is still after this calculation's date, run another
        # step of the calculation
        elif end_date < final_date:
            return predict_weight_change(*input_values)

        else:
            raise ValueError('There is a problem with the dates specified.')


    # === Run code ===

    # Get inital values to feed to recursion function: predict_weight_change
    (
        goal_caloric_difference, goal_energy_intake,
        weight, height, gender, age, physical_activity_levels,
        start_date, calc_period, final_date, dataframe
    ) = get_inital_values(
        ingested_weight,
        ingested_height,
        ingested_physical_activity_levels,
        ingested_gender,
        ingested_age,
        ingested_goal_caloric_difference,
        ingested_body_fat_percentage,
        ingested_start_date,
        ingested_calc_period,
        ingested_calc_length
    )

    # Make weight change prediction
    output = predict_weight_change(
        goal_caloric_difference, goal_energy_intake,
        weight, height, gender, age, physical_activity_levels,
        start_date, calc_period, final_date, dataframe
    )

    # Define a function for turning the output into a dataframe that is usable
    # without the module's unit registry, as it is impossible to convert items
    # between two different unit registries.
    def to_exportable (dataframe):

        # Extract the column headers
        column_names = dataframe.columns.values

        # Add a dict for storing the pint quantities and dimensionality
        unit_dict = {}
        dimensionality_dict = {}

        # Define new dataframe
        exportable_dataframe = pd.DataFrame()

        for header in column_names:

            example_value = dataframe[header][0]

            # If the column is made up of pint quantities...
            if type(example_value) == type(1 * ureg.year):

                # Store the column's units and dimensionality
                unit_dict[header] = str(example_value.units)
                dimensionality_dict[header] = str(example_value.dimensionality)

                # Define new column
                new_column = pd.Series()

                # Add item magnitude as items to new column
                for row in dataframe[header].iteritems():
                    new_column[row[0]] = row[1].magnitude

                # Add new column to new dataframe, with the new header name
                exportable_dataframe[header] = new_column

            # Else if they aren't units, add them unchanged to the new dataframe
            else:
                exportable_dataframe[header] = dataframe[header]

        return (exportable_dataframe, unit_dict, dimensionality_dict)

    exportable_dataframe, unit_dict, dimensionality_dict = to_exportable(output)

    return exportable_dataframe, unit_dict, dimensionality_dict
