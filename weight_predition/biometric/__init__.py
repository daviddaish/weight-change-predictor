# Python 3.6.4

# === Prep for script work with imports and definitions ===
from ..registry import ureg


# === Define basal metabolic rate algorithms ===

# Define basal metabolic rate estimates
# The Revised Harris-Benedict Equation as defined on 29/4/2018 here: https://en.wikipedia.org/wiki/Basal_metabolic_rate
@ureg.check('[mass]', '[length]', '[time]', None)
def revised_harris_benedict_alg (weight, height, age, gender):

    # Define data in equation friendly units
    kg_magnitude = weight.to(ureg.kg).magnitude
    cm_magnitude = height.to(ureg.cm).magnitude
    year_magnitude = age.to(ureg.years).magnitude

    if gender == 'male':
        basal_metabolic_rate_estimate = ((kg_magnitude * 13.397) + (cm_magnitude * 4.799) - (year_magnitude * 5.677) + 88.362) * ureg('kcal / day')

    elif gender == 'female':
        basal_metabolic_rate_estimate = ((kg_magnitude * 9.247) + (cm_magnitude * 3.098) - (year_magnitude * 4.330) + 447.593) * ureg('kcal / day')

    return basal_metabolic_rate_estimate

# The Mifflin St Jeor Equation as defined on 29/4/2018 here: https://en.wikipedia.org/wiki/Basal_metabolic_rate
@ureg.check('[mass]', '[length]', '[time]', None)
def mifflin_st_jeor_alg (weight, height, age, gender):

    # Define data in equation friendly units
    kg_magnitude = weight.to(ureg.kg).magnitude
    cm_magnitude = height.to(ureg.cm).magnitude
    year_magnitude = age.to(ureg.years).magnitude
    gender_modifier = {
        'male': 5.0,
        'female': -161.0
    }[gender]

    basal_metabolic_rate_estimate = ((kg_magnitude * 10.0) + (cm_magnitude * 6.25) - (year_magnitude * 5.0) + gender_modifier) * ureg('kcal / day')

    return basal_metabolic_rate_estimate

# The Katch-McArdle Formula as defined on 29/4/2018 here: https://en.wikipedia.org/wiki/Basal_metabolic_rate
@ureg.check('[mass]', None)
def katch_mcardle_alg (weight, body_fat_percentage):

    # Calculate lean body mass
    lean_body_mass_percentage = 1 - body_fat_percentage
    lean_body_mass = weight * lean_body_mass_percentage

    # Define data in equation friendly units
    lbm_kg_magnitude = lean_body_mass.to(ureg.kg).magnitude

    basal_metabolic_rate_estimate = (370 + (lbm_kg_magnitude * 21.6)) * ureg('kcal / day')
    return basal_metabolic_rate_estimate

# The Cunningham Formula as defined on 29/4/2018 here: http://apjcn.nhri.org.tw/server/APJCN/20/4/646.pdf
@ureg.check('[mass]', None)
def cunningham_alg (weight, body_fat_percentage):

    # Calculate lean body mass
    lean_body_mass_percentage = 1 - body_fat_percentage
    lean_body_mass = weight * lean_body_mass_percentage

    # Define data in equation friendly units
    lbm_kg_magnitude = lean_body_mass.to(ureg.kg).magnitude

    basal_metabolic_rate_estimate = (500 + (lbm_kg_magnitude * 22)) * ureg('kcal / day')
    return basal_metabolic_rate_estimate

# Define function for calculating basal metabolic rate without a body fat percentage
@ureg.check('[mass]', '[length]', '[time]', None)
def get_bmr (weight, height, age, gender):
    basal_metabolic_rate_estimates = []

    # Apply the revised_harris_benedict_alg
    basal_metabolic_rate_estimates.append(revised_harris_benedict_alg(
        weight, height, age, gender
    ))

    # Apply the mifflin_st_jeor_alg
    basal_metabolic_rate_estimates.append(mifflin_st_jeor_alg(
        weight, height, age, gender
    ))

    # Calculate arithmatic mean of BMR erstimates
    mean_basal_metabolic_rate_estimate = sum(basal_metabolic_rate_estimates) / len(basal_metabolic_rate_estimates)

    return mean_basal_metabolic_rate_estimate, basal_metabolic_rate_estimates


# Define function for calculating basal metabolic rate with a body fat percentage
@ureg.check('[mass]', None)
def get_bmr_body_fat_adjusted (weight, body_fat_percentage):
    basal_metabolic_rate_estimates = []

    # Apply the katch_mcardle_alg
    basal_metabolic_rate_estimates.append(katch_mcardle_alg(
        weight, body_fat_percentage
    ))

    # Apply the cunningham_alg
    basal_metabolic_rate_estimates.append(cunningham_alg(
        weight, body_fat_percentage
    ))

    # Calculate arithmatic mean of BMR erstimates
    mean_basal_metabolic_rate_estimate = sum(basal_metabolic_rate_estimates) / len(basal_metabolic_rate_estimates)

    return mean_basal_metabolic_rate_estimate, basal_metabolic_rate_estimates



# === Define total daily energy expenditure calculations ===

# Estimate total daily energy expenditure from pysical activity levels and basal
# metabolic rate, based on "Human energy requirements: Energy Requirement of Adults"
# report as defined on 29/4/2018 here: http://www.fao.org/docrep/007/y5686e/y5686e07.htm
@ureg.check('[energy] / [time]', None)
def estimate_total_daily_energy_expenditure (basal_metabolic_rate, physical_activity_level):
    total_daily_energy_expenditure = basal_metabolic_rate * physical_activity_level

    return total_daily_energy_expenditure

# Calculate TDEE estimates
def calulate_tdee_estimates (basal_metabolic_rate_estimates, physical_activity_level_estimates):

    # Calculate TDEE estimates
    tdee_estimates = []

    for bmr_estimate in basal_metabolic_rate_estimates:
        for pal_value in physical_activity_level_estimates:
            tdee_estimates.append(
                estimate_total_daily_energy_expenditure(bmr_estimate, pal_value)
            )

    mean_tdee_estimate = sum(tdee_estimates) / len(tdee_estimates)

    return mean_tdee_estimate, tdee_estimates



# === Define other biometric algorithms ===

# Define function to calculate body mass index
@ureg.check('[mass]', '[length]')
def get_body_mass_index (weight, height):
    body_mass_index = weight.to(ureg.kg) / height.to(ureg.m) ** 2
    return body_mass_index


# Define function for calculating goal energy intake
def calculate_goal_energy_intake (tdee, energy_modifier):
    goal_energy_intake = tdee * energy_modifier

    return goal_energy_intake

# Calculate the change of weight over a period of time based on energy intake, total
# energy expenditure, and the period of time.
@ureg.check('[energy] / [time]', '[energy] / [time]', '[time]')
def calculate_change_of_weight (total_daily_energy_expenditure, energy_intake_by_time, calc_period):

    # === Calculate rate of tissue change based on intake ===

    # Calculate difference between energy intake by time and TDEE
    energy_intake_difference = energy_intake_by_time - total_daily_energy_expenditure

    # Define human fat tissue's kcal per kg, based off the calories in raw, fresh,
    # seperable pork fat
    human_fat_tissue_energy_density = 7520 * ureg('kcal / kg')

    # Define human muscle tissue's kcal per kg, based off the calories in raw, fresh,
    # pork tenderloin
    human_muscle_tissue_energy_density = 1190 * ureg('kcal / kg')

    # Define what percentage of energy deficit is furfilled by cannibalising fat tissue
    fat_cannibalisation_percentage = 0.95

    # Define what percentage of energy deficit is furfilled by cannibalising muscle tissue
    muscle_cannibalisation_percentage = 1 - fat_cannibalisation_percentage


    if energy_intake_difference <= 0 * ureg('kcal / day'):
        # Find fat tissue weight change over time
        fat_tissue_change_rate = energy_intake_difference / human_fat_tissue_energy_density

        # Find muscle tissue weight loss over time
        # muscle_tissue_loss_rate = (energy_intake_difference * muscle_cannibalisation_percentage) / human_muscle_tissue_energy_density

    else:
        fat_tissue_change_rate = energy_intake_difference / human_fat_tissue_energy_density

    # === Calculate tissue change within step period ===

    # Convert perod and tissue change rates to standard units
    std_fat_tissue_change_rate = fat_tissue_change_rate.to(ureg('kg / day'))
    std_period = calc_period.to(ureg('day'))

    # Calculate rate of weight change
    weight_change_rate = std_fat_tissue_change_rate

    # Calculate tissue change
    fat_tissue_change = std_fat_tissue_change_rate * std_period

    # Define weight changed
    weight_change = fat_tissue_change

    return weight_change, weight_change_rate


    # # === Calculate tissue loss within step period ===
    #
    # # Convert perod and tissue loss rates to standard units
    # std_fat_tissue_loss_rate = fat_tissue_loss_rate.to(ureg('kg / day'))
    # std_muscle_tissue_loss_rate = muscle_tissue_loss_rate.to(ureg('kg / day'))
    # std_period = calc_period.to(ureg('day'))
    #
    # # Calculate rate of weight loss
    # weight_loss_rate = std_fat_tissue_loss_rate + std_muscle_tissue_loss_rate
    #
    # # Calculate tissue loss
    # fat_tissue_loss = std_fat_tissue_loss_rate * std_period
    # muscle_tissue_loss = std_muscle_tissue_loss_rate * std_period
    #
    # # Define weight lost
    # weight_loss = fat_tissue_loss + muscle_tissue_loss
    #
    # return weight_loss, weight_loss_rate
