# === Define constant variables ===


# Physical activity level indexes as defined on 29/4/2018 here: http://www.fao.org/docrep/007/y5686e/y5686e07.htm
physical_activity_levels = (
    {
        'physical activity level': (1.40, 1.5166),
        'description': 'Little to no exercise.'
    },
    {
        'physical activity level': (1.5167, 1.6333),
        'description': 'Light to moderate exercise 1–3 days per week.'
    },
    {
        'physical activity level': (1.6334, 1.7499),
        'description': 'Moderate exercise 3–5 days per week.'
    },
    {
        'physical activity level': (1.75, 1.9999),
        'description': 'One hour of moderate to vigorous exercise every day, or a physically demanding job such as a construction worker.'
    },
    {
        'physical activity level': (2.00, 2.40),
        'description': 'Moderate to vigorous exercise twice per day, in addition to a physically demanding job such as a construction worker.'
    }
)

# Various weight gain or loss goals
goals = (
    {
        'calorie effect': 1.08,
        'description': 'Clean bulk'
    },
    {
        'calorie effect': 1,
        'description': 'Gain muscle, lose fat'
    },
    {
        'calorie effect': 0.95,
        'description': 'Lose fat – 5% calorie reduction'
    },
    {
        'calorie effect': 0.9,
        'description': 'Lose fat – 10% calorie reduction'
    },
    {
        'calorie effect': 0.85,
        'description': 'Lose fat – 15% calorie reduction'
    },
    {
        'calorie effect': 0.8,
        'description': 'Lose fat – 20% calorie reduction'
    },
    {
        'calorie effect': 0.75,
        'description': 'Lose fat – 25% calorie reduction'
    }
)
