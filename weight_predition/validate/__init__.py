# Python 3.6.4

# === Prep for script work with imports and definitions ===
from ..registry import ureg
import dateutil.parser
import datetime



# === Define functions for validating and converting user defined variables ===

# Convert a number and a unit to a Pint lib mass object
def get_weight (unit):
    weight = ureg(unit)
    if (weight.dimensionality != '[mass]'):
        raise ValueError("Weight needs to be set with a mass measurement like kg or lb.")
    return weight

# Convert a number and a unit to a Pint lib length object
def get_height (unit):
    height = ureg(unit)
    if (height.dimensionality != '[length]'):
        raise ValueError("Error! Height needs to be set with a length measurement, like cm or ft.")
    return height

# Define the physical activity level of the user based off their selected index
def get_physical_activity_levels (lower_pal, upper_pal):
    if isinstance(lower_pal, float) and isinstance(upper_pal, float):
        return (lower_pal, upper_pal)

    else:
        raise ValueError('Physical activity level must be a float.')

# Return a string representing gender based off the user's selected index
def get_gender (value):
    male_words = [
        'male',
        'm',
        'man',
        'guy'
        'boy',
        'brother',
        'chap',
        'dude',
        'feller',
        'fellow',
        'gentleman',
        'bloke',
        'mr',
        'mister'
    ]

    female_words = [
        'female',
        'f',
        'woman',
        'lady',
        'girl',
        'gal',
        'sister',
        'gentlewoman',
        'madam',
        'matron',
        'ms',
        'miss',
    ]

    bio_genders = ('male', 'female')

    lower_value = value.lower()

    if lower_value in male_words:
        return bio_genders[0]

    elif lower_value in female_words:
        return bio_genders[1]

    else:
        raise ValueError("I don't recognise the gender you entered.")


# Convert a string of a date into the difference in years between date and now in years
def get_age (birth_date_string):
    birth_date = dateutil.parser.parse(birth_date_string)
    today = datetime.datetime.now()
    days_alive = (today - birth_date).days * ureg.days
    years_alive = days_alive.to(ureg('years'))
    return years_alive

# Return a goal dictionary
def get_goal (value):
    if isinstance(value, float):
        return value

    else:
        raise ValueError('Goal caloric difference must be a float.')


# Convert a string representing a percentage into a float between 0 and 1
def get_body_fat_percentage (perc):
    if isinstance(perc, float):
        body_fat_percentage = perc

    elif isinstance(perc, str):
        try:
            perc_int = int(str(perc).strip('%'))
            body_fat_percentage = float(perc_int) / 100

        except ValueError:
            return False

    elif isinstance(perc, int):
        body_fat_percentage = float(perc) / 100

    else:
        return False

    if body_fat_percentage >= 1.0:
        raise ValueError('It is impossible to have a body fat percentage of 100% or greater.')

    return body_fat_percentage


# Return date object from date string
def get_start_date (date_string):

    if date_string == 'now':
        start_date = datetime.datetime.now()

    else:
        start_date = dateutil.parser.parse(date_string)

    return start_date

# Return period of calculation
def get_calc_period (value):
    period = ureg(value)

    if period.dimensionality == '[time]':
        return period
    else:
        raise ValueError('The calc period is not a measure of time.')

def get_calc_length (value):
    length = ureg(value)

    if length.dimensionality == '[time]':
        return length
    else:
        raise ValueError('The calc length is not a measure of time.')
